#include "engine.hpp"

sf::RenderWindow *Eventable::window;
sf::Event *Eventable::event;
sf::Texture Moveable::textureDelete, Moveable::textureFlip, SceneManager::mBackgroundTexture, SceneManager::mBackgroundTexture2, SceneManager::mBoxWoodenTexture;
sf::Texture SceneManager::mBoxMetalTexture, SceneManager::mBoxCargoTexture, SceneManager::mCheckerTexture, SceneManager::mMagnetTexture;
sf::Texture SceneManager::mPlatformTexture, SceneManager::mBackgroundTexture3;
XMLManager *SceneManager::myXML;
b2World *SceneManager::world;
b2Vec2 *SceneManager::gravity;

int main() {
	sf::RenderWindow window = sf::RenderWindow(sf::VideoMode(940, 515), "Level Editor", sf::Style::Close);
	sf::Event event;
	//window.setActive(false);
	window.setFramerateLimit(60);
	XMLManager xml = XMLManager();
	SceneManager::setXMLManager(xml);
	Eventable::setWindow(window);
	Eventable::setEvent(event);
	b2Vec2 Gravity(0.f, 9.8f);
	b2World World(Gravity);
	SceneManager::setGravity(Gravity);
	SceneManager::setWorld(World);

	if(!arial.loadFromFile("fonts/cour.ttf"))
	{
		return EXIT_FAILURE;
	}

	if(!Moveable::staticInitDelete("gfx/delete.png") ||
		!Moveable::staticInitFlip("gfx/flip.jpg") ||
		!SceneManager::loadTextures())
	{
		//std::cin.get();
		return EXIT_FAILURE;
	}

	SceneManager manager;
	manager = SceneManager();
	/*sf::Thread renderThread(&SceneManager::drawScene, &manager);
	renderThread.launch();*/
	manager.readObjects();
	manager.initEnvironment();
	manager.gameLoop();
	return 0;
}
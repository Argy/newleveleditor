class SceneManager : public Eventable
{
private:
	std::vector<sf::Sprite> environments;
	std::vector<Moveable> objects;
	std::vector<Box> boxOrderList, boxList;
	std::vector<Physical> physicsObjects;
	std::vector<Magnet> magnetsList;
	static sf::Texture mBackgroundTexture, mBoxWoodenTexture, mBoxMetalTexture, mBoxCargoTexture, mMagnetTexture, mCheckerTexture; //static?
	static sf::Texture mBackgroundTexture2, mBackgroundTexture3, mPlatformTexture;
	Grid myGrid;
	Stack myStack;
	static XMLManager* myXML;
	bool editMode, physics, stage3, screen;
	sf::Clock frameTimeClock, FPS;
	int framePerSeconds, magnetsDeleted;
	float frameTime, SCALE;
	static b2Vec2* gravity;
	static b2World* world;
	Line left, right, top;
	b2Body *bodyGround, *bodyGround2;
	Dialog dialogWrite, dialogRead;
	sf::String fileName;
	MenuStrip menuStrip;
	Moveable *lastObject;
	TextBox textX, textY;
	Hint hintX, hintY;
public:
	SceneManager()
	{
		hintX = Hint();
		hintY = Hint();
		magnetsDeleted = 0;
		textX = TextBox();
		textY = TextBox();
		textX.setPosition(sf::Vector2f(250, 0));
		textY.setPosition(sf::Vector2f(500, 0));
		menuStrip = MenuStrip();
		screen = false;
		dialogWrite.setActive(false);
		dialogWrite.setPosition(sf::Vector2f(100, 115));
		dialogRead.setActive(false);
		dialogRead.setPosition(sf::Vector2f(100, 115));
		stage3 = false;
		left.setWidth(3);
		right.setWidth(3);
		top.setWidth(3);
		left.setPosition(sf::Vector2f(100, 35),sf::Vector2f(100, 800));
		right.setPosition(sf::Vector2f(700, 35),sf::Vector2f(700, 800));
		top.setPosition(sf::Vector2f(0, 235),sf::Vector2f(800, 235));
		left.setWidth(3.f);
		right.setWidth(3.f);
		top.setWidth(3.f);
		physics = false;
		SCALE = 30.f;
		editMode = true;
		createGround();
	}

	static void setWorld(b2World &pWorld)
	{
		world = &pWorld;
	}

	static void setGravity(b2Vec2 &pGravity)
	{
		gravity = &pGravity;
	}

	void createGround()
	{
		b2BodyDef bodyGroundDef, bodyGroundDef2, bodyLeftWallDef, bodyRightWallDef;
		bodyGroundDef.position = b2Vec2(400/30.f, 524/30.f);
		bodyGroundDef.type = b2_staticBody;
		bodyGroundDef2.position = b2Vec2(400/30.f, 481/30.f);
		bodyGroundDef2.type = b2_staticBody;
		bodyLeftWallDef.position = b2Vec2(-9/30.f, 275/30.f);
		bodyLeftWallDef.type = b2_staticBody;
		bodyRightWallDef.position = b2Vec2(809/30.f, 275/30.f);
		bodyRightWallDef.type = b2_staticBody;
		bodyGround = world->CreateBody(&bodyGroundDef);
		bodyGround2 = world->CreateBody(&bodyGroundDef2);
		b2Body* bodyLeftWall = world->CreateBody(&bodyLeftWallDef);
		b2Body* bodyRightWall = world->CreateBody(&bodyRightWallDef);
		b2PolygonShape shapeGround, shapeGround2, shapeLeftWall, shapeRightWall;
		shapeGround.SetAsBox((800.f/2)/SCALE, (16.f/2)/SCALE); // Creates a box shape. Divide your desired width and height by 2.
		shapeGround2.SetAsBox((800.f/2)/SCALE, (16.f/2)/SCALE);
		shapeLeftWall.SetAsBox((16.f/2)/SCALE, (800.f/2)/SCALE); // Creates a box shape. Divide your desired width and height by 2.
		shapeRightWall.SetAsBox((16.f/2)/SCALE, (800.f/2)/SCALE); // Creates a box shape. Divide your desired width and height by 2.
		b2FixtureDef fixtureDefGround, fixtureDefGround2, fixtureDefLeftWall, fixtureDefRightWall;
		fixtureDefGround.density = 5.0f;  // Sets the density of the body
		fixtureDefGround.friction = 0.5f;
		fixtureDefGround.shape = &shapeGround; // Sets the shape
		fixtureDefGround2.density = 5.0f;  // Sets the density of the body
		fixtureDefGround2.friction = 0.5f;
		fixtureDefGround2.shape = &shapeGround2; // Sets the shape
		fixtureDefLeftWall.density = 5.0f;  // Sets the density of the body
		fixtureDefLeftWall.friction = 0.5f;
		fixtureDefLeftWall.shape = &shapeLeftWall; // Sets the shape
		fixtureDefRightWall.density = 5.0f;  // Sets the density of the body
		fixtureDefRightWall.friction = 0.5f;
		fixtureDefRightWall.shape = &shapeRightWall; // Sets the shape
		bodyGround->CreateFixture(&fixtureDefGround); // Apply the fixture definition
		bodyGround2->CreateFixture(&fixtureDefGround2);
		bodyLeftWall->CreateFixture(&fixtureDefLeftWall); // Apply the fixture definition
		bodyRightWall->CreateFixture(&fixtureDefRightWall); // Apply the fixture definition
		bodyGround2->SetActive(false);
	}

	void readObjects()
	{
		sf::Vector2f scale;
		int type;
		std::ifstream myFileRead;
		myFileRead.open("objects.stg");
		if (myFileRead.is_open())
		{
			while(myFileRead.good())
			{
				myFileRead >> scale.x >> scale.y >> type; 
				scale.x = scale.x / 1.5f;
				scale.y = scale.y / 1.5f;
				Moveable newItem = Moveable(type);
				switch(type)
				{
				case 0:
					newItem.setTexture(mBoxWoodenTexture);
					break;
				case 1:
					newItem.setTexture(mBoxCargoTexture);
					break;
				case 3:
					newItem.setTexture(mBoxMetalTexture);
					break;
				case 4:
					newItem.setTexture(mCheckerTexture);
					break;
				case 5:
					newItem.setTexture(mMagnetTexture);
					break;
				case 6:
					newItem.setTexture(mPlatformTexture);
					break;
				}
				newItem.setScale(scale);
				myStack.addToList(newItem);
			}
			myFileRead.close();
		}
	}

	void setColor(sf::Color pColor)
	{
		for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
		{
			Box &dObject = *it;
			if(dObject.getType() <= 3)
				dObject.setColor(pColor);
		}
	}

	void resetBoxOrderList()
	{
		boxOrderList.clear();
	}

	static void setXMLManager(XMLManager &pXML)
	{
		myXML = &pXML;
	}

	void setGridStatus(bool status)
	{
		myGrid.setStatus(status);
		myGrid.setSpace(sf::Vector2f(65, 65));
	}

	bool getGridStatus()
	{
		return myGrid.getStatus();
	}

	static bool loadTextures() // static?
	{
		if(!mBackgroundTexture.loadFromFile("gfx/background.png") ||
		!mBackgroundTexture2.loadFromFile("gfx/background2.png") ||
		!mBackgroundTexture3.loadFromFile("gfx/background3.png") ||
		!mBoxWoodenTexture.loadFromFile("gfx/boxwooden.png") ||
		!mBoxMetalTexture.loadFromFile("gfx/boxmetal.png") ||
		!mBoxCargoTexture.loadFromFile("gfx/boxcargo.png") ||
		!mMagnetTexture.loadFromFile("gfx/magnet.png") ||
		!mCheckerTexture.loadFromFile("gfx/checker.png") ||
		!mPlatformTexture.loadFromFile("gfx/platform.png"))
		{
			return EXIT_FAILURE;
		}
	}

	void initEnvironment()
	{
		sf::Sprite background, background2, background3;
		background = sf::Sprite(mBackgroundTexture);
		background.setPosition(0, 35);
		background2 = sf::Sprite(mBackgroundTexture2);
		background2.setPosition(0, 35);
		background2.setColor(sf::Color::Transparent);
		background3 = sf::Sprite(mBackgroundTexture3);
		background3.setPosition(0, 35);
		background3.setColor(sf::Color::Transparent);
		environments.push_back(background);
		environments.push_back(background2);
		environments.push_back(background3);
	}

	void drawScene()
	{
		window->clear();
		for (std::vector<sf::Sprite>::iterator it = environments.begin(); it != environments.end(); ++it)
		{
			sf::Sprite &dSprite = *it;
			if(!(dSprite.getColor() == sf::Color::Transparent))
				window->draw(dSprite);
		}

		myGrid.drawLines();
		left.draw();
		right.draw();
		top.draw();
		for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
		{
			Box &dBox = *it;
			if(physics)
			{
				if(!dBox.isBody())
				{
					dBox.startPhysics(*world, b2BodyType::b2_dynamicBody);
				}
				dBox.update();
			}
			else
			{
				if(dBox.isBody())
				{
					dBox.destroyBody(*world);
					dBox.setRotation(0);
				}	
			}
            dBox.draw();
		}
		for (std::vector<Physical>::iterator it = physicsObjects.begin(); it != physicsObjects.end(); ++it)
		{
			Physical &dPhysical = *it;
			if(physics)
			{
				if(!dPhysical.isBody())
				{
					dPhysical.startPhysics(*world, b2BodyType::b2_staticBody);
				}
				dPhysical.update();
			}
			else
			{
				if(dPhysical.isBody())
				{
					dPhysical.destroyBody(*world);
					dPhysical.setRotation(0);
				}	
			}
			dPhysical.draw();
		}
		for (std::vector<Magnet>::iterator it = magnetsList.begin(); it != magnetsList.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(physics)
			{
				if(!dMagnet.isBody())
				{
					dMagnet.startPhysics(*world, b2BodyType::b2_staticBody);
				}
				dMagnet.update();
			}
			else
			{
				if(dMagnet.isBody())
				{
					dMagnet.destroyBody(*world);
					dMagnet.setRotation(0);
				}	
			}
			dMagnet.draw();
		}
		for (std::vector<Moveable>::iterator it = objects.begin(); it != objects.end(); ++it)
		{
			Moveable &dItem = *it;
			dItem.draw();
		}
		myStack.draw();
		if(dialogWrite.isActive())
			dialogWrite.draw();
		if(dialogRead.isActive())
			dialogRead.draw();
		else
		{
			if(screen)
			{
				sf::Image Screenshot ;
				Screenshot = window->capture() ;
				Screenshot.saveToFile("screens/" + fileName + ".png");
				screen = false;
			}
		}
		menuStrip.drawAll();
		textX.draw();
		textY.draw();
		hintX.draw();
		hintY.draw();
		window->display();
	}

	void controlEventsObjects()
	{
		for (std::vector<Moveable>::iterator it = objects.begin(); it != objects.end(); ++it)
		{
			Moveable &dObject = *it;
			if(editMode==true)
			{
				if(dObject.clickDeleteUp())
				{
					it = objects.erase(it);
					break;
				}
				if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
				{
					if(dObject.clickUp())
					{
						std::ostringstream textik, textik2;
						textik << dObject.getPosition().x;
						textX.setText(textik.str());
						textik2 << dObject.getPosition().y;
						textY.setText(textik2.str());
						lastObject = &dObject;
					}
				}
				dObject.clickFlipUp();
				if(dObject.clickDown())
				{
					break;
				}
				dObject.mouseMove();
				dObject.mouseOnObject();
			}
		}
		for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
		{
			Box &dBox = *it;
			if(editMode==true)
			{
				if(!physics)
				{
					if(dBox.clickDeleteUp())
					{
						if(dBox.isBody())
							dBox.destroyBody(*world);
						it = boxList.erase(it);
						break;
					}
					if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
					{
						if(dBox.clickUp())
						{
							std::ostringstream textik, textik2;
							textik << dBox.getPosition().x;
							textX.setText(textik.str());
							textik2 << dBox.getPosition().y;
							textY.setText(textik2.str());
							lastObject = &dBox;
						}
					}
					dBox.clickFlipUp();
					if(dBox.clickDown())
					{
						break;
					}
					dBox.mouseMove();
					dBox.mouseOnObject();
				}
			}
			else
			{
				if(dBox.clickDown2())
				{
					if(dBox.getType() <= 3)
					{
						dBox.setColor(sf::Color::White);
						boxOrderList.push_back(dBox);
					}
				}
			}
		}
		for (std::vector<Physical>::iterator it = physicsObjects.begin(); it != physicsObjects.end(); ++it)
		{
			Physical &dPhysical = *it;
			if(editMode==true)
			{
				if(!physics)
				{
					if(dPhysical.clickDeleteUp())
					{
						if(dPhysical.isBody())
							dPhysical.destroyBody(*world);
						it = physicsObjects.erase(it);
						break;
					}
					if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
					{
						if(dPhysical.clickUp())
						{
							std::ostringstream textik, textik2;
							textik << dPhysical.getPosition().x;
							textX.setText(textik.str());
							textik2 << dPhysical.getPosition().y;
							textY.setText(textik2.str());
							lastObject = &dPhysical;
						}
					}
					dPhysical.clickFlipUp();
					if(dPhysical.clickDown())
					{
						break;
					}
					dPhysical.mouseMove();
					dPhysical.mouseOnObject();
				}
			}
		}
		for (std::vector<Magnet>::iterator it = magnetsList.begin(); it != magnetsList.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(editMode==true)
			{
				if(!physics)
				{
					if(dMagnet.clickDeleteUp())
					{
						for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
						{
							Box &dBox = *it;
							dBox.deleteMagnet(dMagnet.getId());
						}
						if(dMagnet.isBody())
							dMagnet.destroyBody(*world);
						it = magnetsList.erase(it);
						magnetsDeleted++;
						break;
					}
					if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
					{
						if(dMagnet.clickUp())
						{
							std::ostringstream textik, textik2;
							textik << dMagnet.getPosition().x;
							textX.setText(textik.str());
							textik2 << dMagnet.getPosition().y;
							textY.setText(textik2.str());
							lastObject = &dMagnet;
							for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
							{
								Box &dBox = *it;
								dBox.setPosMagnet(dMagnet.getId(), dMagnet.getPosition());
							}
						}
					}
					if(dMagnet.clickFlipUp())
					{
						if(dMagnet.getScale().x < 0)
						{
							dMagnet.setDirection(-1);
						}
						else
						{
							dMagnet.setDirection(1);
						}
						for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
						{
							Box &dBox = *it;
							dBox.flipMagnet(dMagnet.getId());
						}
					}
					if(dMagnet.clickDown())
					{
						break;
					}
					dMagnet.mouseMove();
					dMagnet.mouseOnObject();
				}
				else
				{
					if(dMagnet.clickDown2())
					{
						if(!dMagnet.isActive())
						{
							dMagnet.setColor(sf::Color::Green);
							dMagnet.setActive(true);
						}
						else
						{
							dMagnet.setColor(sf::Color::White);
							dMagnet.setActive(false);
							for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
							{
								Box &dBox = *it;
								if(dBox.getType() == 3)
								{
									if(dBox.getPosition().y < dMagnet.getPosition().y && dBox.getPosition().y > dMagnet.getPosition().y - 30 ||
									dBox.getPosition().y > dMagnet.getPosition().y && dBox.getPosition().y < dMagnet.getPosition().y + 30)
									{
										if((dMagnet.getDirection() == -1 && dMagnet.getPosition().x < dBox.getPosition().x) || (dMagnet.getDirection() == 1 && dMagnet.getPosition().x > dBox.getPosition().y))
										{
											if(dBox.getMagnetism())
												dBox.magnetOff(dMagnet.getId(), dMagnet.getDirection());
										}
									}
								}
							}
						}
					}
				}
			}
		}
		guiControl();
	}

	void magnetism()
	{
		for (std::vector<Magnet>::iterator it = magnetsList.begin(); it != magnetsList.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(dMagnet.isActive())
			{
				for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
				{
					Box &dBox = *it;
					if(dBox.getType() == 3)
					{
						if(dBox.getPosition().y < dMagnet.getPosition().y && dBox.getPosition().y > dMagnet.getPosition().y - 30 ||
								dBox.getPosition().y > dMagnet.getPosition().y && dBox.getPosition().y < dMagnet.getPosition().y + 30)
						{
							if((dMagnet.getDirection() == -1 && dMagnet.getPosition().x < dBox.getPosition().x) || (dMagnet.getDirection() == 1 && dMagnet.getPosition().x > dBox.getPosition().x))
							{
								dBox.magnetOn(dMagnet.getId(), dMagnet.getDirection(), abs(dMagnet.getPosition().x - dBox.getPosition().x));
							}
							else
							{
								dBox.magnetOff(dMagnet.getId(), dMagnet.getDirection());
							}
						}
						else
						{
							dBox.magnetOff(dMagnet.getId(), dMagnet.getDirection());
						}
					}
				}
			}
		}
	}

	void guiControl()
	{
		if(dialogWrite.isActive())
		{
			if(dialogWrite.isTextBoxActive())
				dialogWrite.writing();
			dialogWrite.onClick();
			if(dialogWrite.onButtonClick())
			{
				fileName = dialogWrite.getString();
				myXML->write(fileName);
				dialogWrite.setActive(false);
				screen = true;
				this->setColor(sf::Color::White);
				this->resetBoxOrderList();
				editMode = true;
			}
			dialogWrite.mouseOnObject();
		}
		if(dialogRead.isActive())
		{
			if(dialogRead.isTextBoxActive())
				dialogRead.writing();
			dialogRead.onClick();
			if(dialogRead.onButtonClick())
			{
				fileName = dialogRead.getString();
				myXML->read(fileName);
				dialogRead.setActive(false);
				screen = true;
				this->setColor(sf::Color::White);
				this->resetBoxOrderList();
				editMode = true;
				clearScene();
				loadScene();
			}
			dialogRead.mouseOnObject();
		}
		if(textX.isActive())
		{
			if(textX.mouseOnObject())
			{
				hintX.setString("X pozice nakliknuteho objektu.");
				hintX.setPosition(sf::Vector2f(textX.getPosition().x + 20, textX.getPosition().y + 80));
				hintX.show(true);
			}
			else
			{
				hintX.show(false);
			}
			textX.writing();
			if(!textX.onClick())
			{
				if(lastObject != 0)
				{
					std::istringstream textoveX(textX.getString()), textoveY(textY.getString());
					float x, y;
					textoveX >> x;
					textoveY >> y;
					lastObject->setPposition(sf::Vector2f(x, y));
				}
			}
		}
		else
		{
			if(textX.mouseOnObject())
			{
				hintX.setString("X pozice nakliknuteho objektu.");
				hintX.setPosition(sf::Vector2f(textX.getPosition().x + 20, textX.getPosition().y + 80));
				hintX.show(true);
			}
			else
			{
				hintX.show(false);
			}
			textX.onClick();
		}
		if(textY.isActive())
		{
			if(textY.mouseOnObject())
			{
				hintY.setString("Y pozice nakliknuteho objektu.");
				hintY.setPosition(sf::Vector2f(textY.getPosition().x + 20, textY.getPosition().y + 80));
				hintY.show(true);
			}
			else
			{
				hintY.show(false);
			}
			textY.writing();
			if(!textY.onClick())
			{
				if(lastObject != 0)
				{
					std::istringstream textoveX(textX.getString()), textoveY(textY.getString());
					float x, y;
					textoveX >> x;
					textoveY >> y;
					lastObject->setPposition(sf::Vector2f(x, y));
				}
			}
		}
		else
		{
			if(textY.mouseOnObject())
			{
				hintY.setString("Y pozice nakliknuteho objektu.");
				hintY.setPosition(sf::Vector2f(textY.getPosition().x + 20, textY.getPosition().y + 80));
				hintY.show(true);
			}
			else
			{
				hintY.show(false);
			}
			textY.onClick();
		}
	}

	void controlLoop()
	{
		while (window->pollEvent(*event))
		{
			controlEventsObjects();
			if (event->type == sf::Event::Closed)
				window->close();

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::Escape))
                window->close();

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::Tab))
			{
				if(textX.isActive())
				{
					if(lastObject != 0)
					{
						std::istringstream textoveX(textX.getString()), textoveY(textY.getString());
						float x, y;
						textoveX >> x;
						textoveY >> y;
						lastObject->setPposition(sf::Vector2f(x, y));
					}
					textX.setActive(false);
					textY.setActive(true);
				}
				else if(textY.isActive())
				{
					if(lastObject != 0)
					{
						std::istringstream textoveX(textX.getString()), textoveY(textY.getString());
						float x, y;
						textoveX >> x;
						textoveY >> y;
						lastObject->setPposition(sf::Vector2f(x, y));
					}
					textY.setActive(false);
					textX.setActive(true);
				}
			}

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::Return))
			{
				if(dialogWrite.isActive())
				{
					fileName = dialogWrite.getString();
					myXML->write(fileName);
					dialogWrite.setActive(false);
					screen = true;
					this->setColor(sf::Color::White);
					this->resetBoxOrderList();
					editMode = true;
				}
				if(dialogRead.isActive())
				{
					fileName = dialogRead.getString();
					myXML->read(fileName);
					dialogRead.setActive(false);
					screen = true;
					this->setColor(sf::Color::White);
					this->resetBoxOrderList();
					editMode = true;
					clearScene();
					loadScene();
				}
				else if(textX.isActive())
				{
					std::istringstream textoveX(textX.getString()), textoveY(textY.getString());
					float x, y;
					textoveX >> x;
					textoveY >> y;
					lastObject->setPposition(sf::Vector2f(x, y));
				}
				else if(textY.isActive())
				{
					std::istringstream textoveX(textX.getString()), textoveY(textY.getString());
					float x, y;
					textoveX >> x;
					textoveY >> y;
					lastObject->setPposition(sf::Vector2f(x, y));
				}
			}

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::G))
			{
				if(!dialogWrite.isTextBoxActive() && !dialogRead.isTextBoxActive())
				{
					if(myGrid.getStatus() == false)
					{
						myGrid.setStatus(true);
						myGrid.setSpace(sf::Vector2f(65, 65));
						break;
					}
					else
					{
						myGrid.setStatus(false);
						break;
					}
				}
			}

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::F2))
			{
				changeBackground();
			}

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::F3))
				runPhysics();

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::F4))
                clearScene();

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::F5))
			{
				dialogRead.setActive(true);
				dialogRead.setLabel("Zadejte nazev souboru: ");
			}

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::F7))
				boxPicking();

			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::F8))
				saveScene();

			if ((event->type == sf::Event::MouseButtonPressed) && (event->mouseButton.button == sf::Mouse::Right))
			{
				sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
				Moveable newItem = myStack.getSelectedItem();
				newItem.setScale(sf::Vector2f(newItem.getScale().x * 1.5f, newItem.getScale().y * 1.5f));
				newItem.setPposition(MousePos);
				if(newItem.getType() <= 3)
				{
					Box box;
					if(physics)
					{
						box = Box(MousePos, magnetsList);
						box.setTexture(*newItem.getTexture());
						box.setScale(newItem.getScale());
						box.setType(newItem.getType());
						box.startPhysics(*world, b2BodyType::b2_dynamicBody);
					}
					else
					{
						box = Box(magnetsList);
						box.setTexture(*newItem.getTexture());
						box.setScale(newItem.getScale());
						box.setPposition(MousePos);
						box.setType(newItem.getType());
					}
					boxList.push_back(box);
				}
				else if(newItem.getType() == 5)
				{
					Magnet magnet;
					if(physics)
					{
						magnet = Magnet(MousePos, magnetsList.size() + magnetsDeleted);
						magnet.setTexture(*newItem.getTexture());
						magnet.setScale(newItem.getScale());
						magnet.setType(newItem.getType());
						magnet.startPhysics(*world, b2BodyType::b2_staticBody);
					}
					else
					{
						magnet = Magnet(magnetsList.size() + magnetsDeleted);
						magnet.setTexture(*newItem.getTexture());
						magnet.setScale(newItem.getScale());
						magnet.setPposition(MousePos);
						magnet.setType(newItem.getType());
					}
					for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
					{
						Box &dBox = *it;
						dBox.addMagnet(magnet);
					}
					magnetsList.push_back(magnet);
				}
				else if(newItem.getType() == 6)
				{
					Platform platform;
					if(physics)
					{
						platform = Platform(MousePos);
						platform.setTexture(*newItem.getTexture());
						platform.setScale(newItem.getScale());
						platform.setType(newItem.getType());
						platform.startPhysics(*world, b2BodyType::b2_staticBody);
					}
					else
					{
						platform = Platform();
						platform.setTexture(*newItem.getTexture());
						platform.setScale(newItem.getScale());
						platform.setPposition(MousePos);
						platform.setType(newItem.getType());
					}
					physicsObjects.push_back(platform);
				}
				else
				{
					objects.push_back(newItem);
				}
			}

			myStack.controlEvents();
			if(menuStrip.isShow())
			{
				if(menuStrip.buttonClickDown(0))
				{
					changeBackground();
				}
				else if(menuStrip.buttonClickDown(1))
				{
					runPhysics();
				}
				else if(menuStrip.buttonClickDown(2))
				{
					clearScene();
				}
				else if(menuStrip.buttonClickDown(3))
				{
					dialogRead.setActive(true);
					dialogRead.setLabel("Zadejte nazev souboru: ");
				}
				else if(menuStrip.buttonClickDown(4))
				{
					boxPicking();
				}
				else if(menuStrip.buttonClickDown(5))
				{
					saveScene();
				}
			}
			menuStrip.control();
		}
	}

	void gameLoop()
	{
		while(window->isOpen())
		{
			frameTime = frameTimeClock.restart().asSeconds();
			controlLoop();
			if(physics)
			{
				world->Step(1/60.f, 8, 3);
				magnetism();
				for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
				{
					Box &dBox = *it;
					if(stage3)
					{
						dBox.underWaterEffect();
					}
					if(dBox.getType() == 3)
							dBox.magnetism(stage3);
				}
			}
			drawScene();
			framePerSeconds++;
			if(FPS.getElapsedTime().asSeconds() > 1)
			{
				std::cout << "FPS: " << framePerSeconds << std::endl;
				std::cout << "Size: " << boxList.size() << std::endl;
				framePerSeconds = 0;
				FPS.restart();
			}
		}
	}

	void clearScene()
	{
		objects.clear();
		boxOrderList.clear();
		for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
		{
			Box &dBox = *it;
			if(dBox.isBody())
				dBox.destroyBody(*world);
		}
		boxList.clear();
		for (std::vector<Physical>::iterator it = physicsObjects.begin(); it != physicsObjects.end(); ++it)
		{
			Physical &dPhysical = *it;
			if(dPhysical.isBody())
				dPhysical.destroyBody(*world);
		}
		physicsObjects.clear();
		for (std::vector<Magnet>::iterator it = magnetsList.begin(); it != magnetsList.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(dMagnet.isBody())
				dMagnet.destroyBody(*world);
		}
		magnetsList.clear();
		magnetsDeleted = 0;
	}

	void changeBackground()
	{
		int i = 0, j = 0;

		for (std::vector<sf::Sprite>::iterator it = environments.begin(); it != environments.end(); ++it)
		{
			sf::Sprite &dSprite = *it;
			i++;
			if(i == j)
			{
				dSprite.setColor(sf::Color::White);
				if(i == environments.size())
				{
					left.setPosition(sf::Vector2f(50, 35),sf::Vector2f(50, 800));
					right.setPosition(sf::Vector2f(750, 35),sf::Vector2f(750, 800));
					top.setPosition(sf::Vector2f(0, 223),sf::Vector2f(800, 223));
					stage3 = true;
				}
				else
				{
					left.setPosition(sf::Vector2f(100, 35),sf::Vector2f(100, 800));
					right.setPosition(sf::Vector2f(700, 35),sf::Vector2f(700, 800));
					top.setPosition(sf::Vector2f(0, 235),sf::Vector2f(800, 235));
					stage3 = false;
				}
				break;
			}
			if(dSprite.getColor() == sf::Color::White) {
				dSprite.setColor(sf::Color::Transparent);
				j = i + 1;
				if(j > environments.size())
				{
					left.setPosition(sf::Vector2f(100, 35),sf::Vector2f(100, 800));
					right.setPosition(sf::Vector2f(700, 35),sf::Vector2f(700, 800));
					top.setPosition(sf::Vector2f(0, 235),sf::Vector2f(800, 235));
					stage3 = false;
					std::vector<sf::Sprite>::iterator it2 = environments.begin();
					sf::Sprite &dSprite2 = *it2;
					dSprite2.setColor(sf::Color::White);
				}
			}
		}
	}

	void saveScene()
	{
		if(editMode == false)
		{
			myXML->setObjects(objects);
			myXML->setBoxes(boxOrderList);
			myXML->setPhysicsObjects(physicsObjects);
			myXML->setMagnets(magnetsList);
			dialogWrite.setActive(true);
			dialogWrite.setLabel("Zadejte nazev souboru: ");
		}
		else
		{
			std::cout << "Prvne musite dat F7 a vybrat poradi boxu." << std::endl;
		}
	}

	void loadScene()
	{
		boxList = myXML->getBoxes();
		magnetsList = myXML->getMagnets();
		objects = myXML->getCheckers();
		physicsObjects = myXML->getPlatforms();
		for (std::vector<Physical>::iterator it = physicsObjects.begin(); it != physicsObjects.end(); ++it)
		{
			Physical &dPlatform = *it;
			dPlatform.setTexture(mPlatformTexture);
		}
		for (std::vector<Moveable>::iterator it = objects.begin(); it != objects.end(); ++it)
		{
			Moveable &dMoveable = *it;
			dMoveable.setTexture(mCheckerTexture);
		}
		for (std::vector<Box>::iterator it = boxList.begin(); it != boxList.end(); ++it)
		{
			Box &dBox = *it;
			switch(dBox.getType())
			{
			case 0:
				dBox.setTexture(mBoxWoodenTexture);
				break;
			case 1:
				dBox.setTexture(mBoxCargoTexture);
				break;
			case 3:
				dBox.setTexture(mBoxMetalTexture);
				break;
			}
		}
		for (std::vector<Magnet>::iterator it = magnetsList.begin(); it != magnetsList.end(); ++it)
		{
			Magnet &dMagnet = *it;
			dMagnet.setTexture(mMagnetTexture);
		}
	}

	void boxPicking()
	{
		if(editMode==true)
		{
			editMode = false;
			this->setColor(sf::Color::Red);
		}
		else
		{
			this->setColor(sf::Color::White);
			this->resetBoxOrderList();
			editMode = true;
		}
	}

	void runPhysics()
	{
		if(physics == false)
		{
			std::vector<sf::Sprite>::iterator it = environments.end();
			sf::Sprite &dSprite = *it;
			if(stage3)
			{
				world->SetGravity(b2Vec2(0.f, 3.3f));
				bodyGround->SetActive(false);
				bodyGround2->SetActive(true);
			}
			else
			{
				world->SetGravity(b2Vec2(0.f, 9.8f));
				bodyGround->SetActive(true);
				bodyGround2->SetActive(false);
			}
			physics = true;
		}
		else
		{
			physics = false;
		}
	}
};
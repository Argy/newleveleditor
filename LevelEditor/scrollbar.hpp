class Scrollbar : public Eventable
{
private:
	int mPos, mItems;
	//sf::Vector2f mPosition, mSliderSize, mSize;
	sf::RectangleShape shapeScrollbar, shape;
	sf::Vector2f startMovePosition, endMovePosition;
	bool mousePressed;
public:
	Scrollbar()
	{
		mPos = 1;
		mItems = 1;
		shape.setFillColor(sf::Color::Red);
		shapeScrollbar.setFillColor(sf::Color::Yellow);
	}

	Scrollbar(int pItems)
	{
		mPos = 1;
		mItems = pItems - 1;
		shape.setFillColor(sf::Color::Red);
		shapeScrollbar.setFillColor(sf::Color::Yellow);
	}

	void setPosition(float pX, float pY)
	{
		shape.setPosition(pX, pY);
		shapeScrollbar.setPosition(pX, pY);
	}

	void setPosition(sf::Vector2f pPosition)
	{
		shape.setPosition(pPosition);
		shapeScrollbar.setPosition(pPosition);
	}

	void setSize(float pWidth, float pHeight)
	{
		shape.setSize(sf::Vector2f(pWidth, pHeight));
		shapeScrollbar.setSize(sf::Vector2f(pWidth, pHeight / mItems));
	}

	void setSize(sf::Vector2f pSize)
	{
		shape.setSize(pSize);
		shapeScrollbar.setSize(sf::Vector2f(pSize.x, pSize.y / mItems));
	}

	void setItems(int pItems)
	{
		mItems = pItems - 1;
	}

	sf::Vector2f getPosition()
	{
		return shape.getPosition();
	}

	sf::Vector2f getSize()
	{
		return shape.getSize();
	}

	int getItems()
	{
		return mItems + 1;
	}

	int getPos()
	{
		return mPos;
	}

	void draw()
	{
		window->draw(shape);
		window->draw(shapeScrollbar);
	}

	bool mouseDown()
	{
		if ((event->type == sf::Event::MouseButtonPressed) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f mousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(mousePos.x > shapeScrollbar.getPosition().x && mousePos.y > shapeScrollbar.getPosition().y &&
			mousePos.x < (shapeScrollbar.getPosition().x + shapeScrollbar.getSize().x) &&
			mousePos.y < (shapeScrollbar.getPosition().y + shapeScrollbar.getSize().y))
			{
				//startMovePosition = mousePos;
				mousePressed = true;
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	bool mouseUp()
	{
		if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
		{
			/*sf::Vector2f mousePos = mWindow->convertCoords(event->mouseButton.x, event->mouseButton.y);
			endMovePosition = mousePos;
			std::cout << "endMovePosition.y: " << endMovePosition.y << std::endl;*/
			mousePressed = false;
			return true;
		}
	}

	bool mouseMove()
	{
		if ((event->type == sf::Event::MouseMoved) && (mousePressed == true))
		{
			sf::Vector2f mousePos = sf::Vector2f(event->mouseMove.x, event->mouseMove.y);
			if((mousePos.y > shape.getPosition().y) || (mousePos.y < (shape.getPosition().y + mItems * shapeScrollbar.getSize().y)))
			{
				if(((mousePos.y - shape.getPosition().y) / shapeScrollbar.getSize().y) >= 0 && ((mousePos.y - shape.getPosition().y) / shapeScrollbar.getSize().y) <= mItems)
				{
					mPos = ((mousePos.y - shape.getPosition().y) / shapeScrollbar.getSize().y);
					shapeScrollbar.setPosition(shape.getPosition().x, shape.getPosition().y + mPos * shapeScrollbar.getSize().y);
					mPos+=1;
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}



	void mouseWheel(int pos)
	{
		if((pos - 1 >= 0) && (pos - 1 < mItems))
		mPos = pos - 1;
		shapeScrollbar.setPosition(shape.getPosition().x, shape.getPosition().y + mPos * shapeScrollbar.getSize().y);
	}
};

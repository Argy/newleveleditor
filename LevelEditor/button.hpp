class Button : public Eventable, public sf::RectangleShape
{
private:
	sf::Text title;
	int type;
public:
	Button()
	{
		type = 0;
		title.setFont(arial);
		title.setColor(sf::Color::Black);
	}

	Button(int typ)
	{
		type = typ;
		title.setFont(arial);
		title.setColor(sf::Color::Black);
	}

	void setPosition(sf::Vector2f pos)
	{
		sf::RectangleShape::setPosition(pos);
		title.setPosition(pos);
	}

	void setTitle(sf::String string)
	{
		title.setString(string);
	}

	void setSize(sf::Vector2f siz)
	{
		sf::RectangleShape::setSize(siz);
		title.setCharacterSize(16);
	}

	bool mouseOnObject()
	{
		if(type == 1)
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseMove.x, event->mouseMove.y);
			if(MousePos.x > this->getPosition().x &&
				MousePos.y > this->getPosition().y &&
				MousePos.x < (this->getPosition().x + this->getSize().x) &&
				MousePos.y < (this->getPosition().y + this->getSize().y))
			{
				this->setFillColor(sf::Color(255, 255, 255));
				return true;
			}
			else
			{
				this->setFillColor(sf::Color(150, 150, 150));
				return false;
			}
		}
	}

	bool clickUp()
	{
		if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > this->getPosition().x &&
				MousePos.y > this->getPosition().y &&
				MousePos.x < (this->getPosition().x + this->getSize().x) &&
				MousePos.y < (this->getPosition().y + this->getSize().y))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	bool clickDown()
	{
		if ((event->type == sf::Event::MouseButtonPressed) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > this->getPosition().x &&
				MousePos.y > this->getPosition().y &&
				MousePos.x < (this->getPosition().x + this->getSize().x) &&
				MousePos.y < (this->getPosition().y + this->getSize().y))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	void draw()
	{
		window->draw(*this);
		window->draw(title);
	}
};
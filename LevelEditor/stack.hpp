class Stack : public Eventable
{
private:
	int mPos;
	std::vector<Item> itemList;
	sf::RectangleShape listInterface;
	Scrollbar mScrollbar;
	Item *selectedItem;
public:
	Stack()
	{
		listInterface.setPosition(window->getSize().x - 140, 40);
		listInterface.setSize(sf::Vector2f(140, 400));
		listInterface.setFillColor(sf::Color::White);
		mScrollbar.setPosition(listInterface.getPosition().x + listInterface.getSize().x - 20, listInterface.getPosition().y);
		mPos = 1;
		/*Item test;
		selectedItem = &test;*/
	}

	void addToList(Moveable pItem)
	{
		Item newItem = Item(sf::Vector2f(listInterface.getPosition().x, listInterface.getPosition().y + (listInterface.getSize().y / 5) * itemList.size()), pItem);
		newItem.setSize(sf::Vector2f(listInterface.getSize().x - 20, listInterface.getSize().y / 5));
		newItem.setItemScale(pItem.getScale());
		if(itemList.size() == 0)
			newItem.select();
		itemList.push_back(newItem);
		mScrollbar.setItems(itemList.size() - 3);
		mScrollbar.setSize(20, listInterface.getSize().y);
	}

	void controlEvents()
	{

		if ((event->type == sf::Event::MouseWheelMoved))
		{
			if(mPos - event->mouseWheel.delta >= 1)
			{
				if(mPos - event->mouseWheel.delta < mScrollbar.getItems())
				{
					mPos -= event->mouseWheel.delta;
					mScrollbar.mouseWheel(mPos);
				}
			}
			int i = 1;
			for (std::vector<Item>::iterator it = itemList.begin(); it != itemList.end(); ++it)
			{
				if(i < mPos + 5 && i >= mPos)
				{
					Item &dItem = *it;
					dItem.setPosition(sf::Vector2f(listInterface.getPosition().x, listInterface.getPosition().y + (listInterface.getSize().y / 5) * (i - mPos)));
					//std::cout << "i" << i << "x:" << listInterface.getPosition().x << " y:" << listInterface.getPosition().y + (listInterface.getSize().y / 5) * (i - mPos) << std::endl;
				}
				i++;
			}
		}

		mScrollbar.mouseUp();
		mScrollbar.mouseDown();
		if(mScrollbar.mouseMove())
		{
			mPos = mScrollbar.getPos();
			int i = 1;
			for (std::vector<Item>::iterator it = itemList.begin(); it != itemList.end(); ++it)
			{
				if(i < mPos + 5 && i >= mPos)
				{
					Item &dItem = *it;
					dItem.setPosition(sf::Vector2f(listInterface.getPosition().x, listInterface.getPosition().y + (listInterface.getSize().y / 5) * (i - mPos)));
					//std::cout << "i" << i << "x:" << listInterface.getPosition().x << " y:" << listInterface.getPosition().y + (listInterface.getSize().y / 5) * (i - mPos) << std::endl;
				}
				i++;
			}
		}
		int i = 1;
		for (std::vector<Item>::iterator it = itemList.begin(); it != itemList.end(); ++it)
		{
			if(i < mPos + 5 && i >= mPos)
			{
				Item &dItem = *it;
				if(dItem.clickDown())
				{
					for (std::vector<Item>::iterator it = itemList.begin(); it != itemList.end(); ++it)
					{
						Item &dItem = *it;
						if(!dItem.clickDown() && dItem.getSelect())
						{
							dItem.select();
						}
					}
					dItem.select();
					selectedItem = &dItem;
				}
			}
			i++;
		}
	}

	Moveable getSelectedItem()
	{
		if(selectedItem == 0)
		{
			std::vector<Item>::iterator it = itemList.begin();
			Item &test = *it;
			selectedItem = &test;
		}
			return selectedItem->getItem();
	}

	void draw()
	{
		window->draw(listInterface);
		int i = 1;
		for (std::vector<Item>::iterator it = itemList.begin(); it != itemList.end(); ++it)
		{
			if(i < mPos + 5 && i >= mPos)
			{
				Item &dItem = *it;
				dItem.draw();
				//std::cout << "i:" << i << std::endl;
			}
			i++;
		}
		mScrollbar.draw();
	}
};
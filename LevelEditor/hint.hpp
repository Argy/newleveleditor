class Hint : public Eventable
{
private:
	bool mShow;
	sf::RectangleShape border;
	sf::Text text;
public:
	Hint()
	{
		mShow = false;
		text.setCharacterSize(20);
		text.setFont(arial);
		text.setColor(sf::Color::Black);
		border.setSize(sf::Vector2f(100, 50));
		border.setOutlineColor(sf::Color::Black);
		border.setFillColor(sf::Color(200, 200, 200));
		border.setOutlineThickness(1);
	}

	void setString(sf::String buffer)
	{
		text.setString(buffer);
		border.setSize(sf::Vector2f((buffer.getSize() + 2) * 12, 30));
	}

	void setPosition(sf::Vector2f pos)
	{
		border.setPosition(pos);
		text.setPosition(pos.x + 5, pos.y + 5);
	}

	void show(bool status)
	{
		mShow = status;
	}

	void draw()
	{
		if(mShow)
		{
			window->draw(border);
			window->draw(text);
		}
	}
};
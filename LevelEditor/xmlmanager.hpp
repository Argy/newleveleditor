class XMLManager
{
private:
	std::string fileName;
	std::ofstream myFileWrite;
	std::ifstream myFileRead;
	std::vector<Moveable> objectsList;
	std::vector<Moveable> wrListCheckers, rdListCheckers;
	std::vector<Magnet> wrListMagnets, rdListMagnets;
	std::vector<Box> wrListBoxes, rdListBoxes;
	std::vector<Physical> wrListPlatforms, rdListPlatforms;
public:
	XMLManager()
	{
		fileName = "level-001-001.lvl";
	}

	void setBoxes(std::vector<Box> pBoxesList)
	{
		Box boxik;
		for (std::vector<Box>::iterator it = pBoxesList.begin(); it != pBoxesList.end(); ++it)
		{
			Box dObject = *it;
			switch(dObject.getType())
			{
			case 0:
				boxik = Box();
				boxik.setTexture(*dObject.getTexture());
				boxik.setPosition(dObject.getPosition());
				boxik.setScale(dObject.getScale());
				boxik.setType(0);
				wrListBoxes.push_back(boxik);
				break;
			case 1:
				boxik = Box();
				boxik.setTexture(*dObject.getTexture());
				boxik.setPosition(dObject.getPosition());
				boxik.setScale(dObject.getScale());
				boxik.setType(1);
				wrListBoxes.push_back(boxik);
				break;
			case 3:
				boxik = Box();
				boxik.setTexture(*dObject.getTexture());
				boxik.setPosition(dObject.getPosition());
				boxik.setScale(dObject.getScale());
				boxik.setType(3);
				wrListBoxes.push_back(boxik);
				break;
			}
		}
	}

	void setObjects(std::vector<Moveable> pObjectsList)
	{
		objectsList = pObjectsList;
		Checker check;
		for (std::vector<Moveable>::iterator it = objectsList.begin(); it != objectsList.end(); ++it)
		{
			Moveable dObject = *it;
			switch(dObject.getType())
			{
			case 4:
				check = Checker();
				check.setTexture(*dObject.getTexture());
				check.setPosition(dObject.getPosition());
				check.setScale(dObject.getScale());
				wrListCheckers.push_back(check);
				break;
			}
		}
	}

	void setPhysicsObjects(std::vector<Physical> pPhysicsObjectsList)
	{
		Platform platf;
		for (std::vector<Physical>::iterator it = pPhysicsObjectsList.begin(); it != pPhysicsObjectsList.end(); ++it)
		{
			Physical dObject = *it;
			switch(dObject.getType())
			{
			case 6:
				platf = Platform();
				platf.setTexture(*dObject.getTexture());
				platf.setPosition(dObject.getPosition());
				platf.setScale(dObject.getScale());
				wrListPlatforms.push_back(platf);
				break;
			}
		}
	}

	void setMagnets(std::vector<Magnet> pMagnetsList)
	{
		for (std::vector<Magnet>::iterator it = pMagnetsList.begin(); it != pMagnetsList.end(); ++it)
		{
			Magnet magn;
			Magnet dObject = *it;
			magn = Magnet();
			magn.setTexture(*dObject.getTexture());
			magn.setPosition(dObject.getPosition());
			magn.setScale(dObject.getScale());
			if(dObject.getScale().x < 0)
			{
				magn.setDirection(1);
			}
			else
			{
				magn.setDirection(0);
			}
			wrListMagnets.push_back(magn);
		}
	}

	std::vector<Moveable> getCheckers()
	{
		return rdListCheckers;
	}

	std::vector<Box> getBoxes()
	{
		return rdListBoxes;
	}

	std::vector<Magnet> getMagnets()
	{
		return rdListMagnets;
	}

	std::vector<Physical> getPlatforms()
	{
		return rdListPlatforms;
	}

	void read(std::string lineRead)
	{
		rdListBoxes.clear();
		rdListCheckers.clear();
		rdListMagnets.clear();
		rdListPlatforms.clear();
		fileName = "levels/" + lineRead;
		myFileRead.open(fileName);
		if (myFileRead.is_open())
		{
			while(myFileRead.good())
			{
				std::getline(myFileRead, lineRead);
				if(lineRead.find("checker") < lineRead.size())
				{
					float x = 0, y = 0;
					if(lineRead.find("x=") < lineRead.size())
					{
						x += atof(&lineRead[lineRead.find("x=") + 3]);
					}

					if(lineRead.find("y=") < lineRead.size())
					{
						y += atof(&lineRead[lineRead.find("y=") + 3]);
					}
					Checker newChecker;
					newChecker = Checker();
					newChecker.setType(4);
					newChecker.setPposition(sf::Vector2f(x, y));
					std::cout << "x: " << x << "y: " << y << std::endl;
					rdListCheckers.push_back(newChecker);
					std::cout << rdListCheckers.size() << std::endl;
				}
				else if(lineRead.find("magnet") < lineRead.size())
				{
					float x = 0, y = 0;
					int direction = 0;
					if(lineRead.find("x=") < lineRead.size())
					{
						x += atof(&lineRead[lineRead.find("x=") + 3]);
					}

					if(lineRead.find("y=") < lineRead.size())
					{
						y += atof(&lineRead[lineRead.find("y=") + 3]);
					}

					if(lineRead.find("direction=") < lineRead.size())
					{
						direction += atof(&lineRead[lineRead.find("direction=") + 11]);
					}
					Magnet newMagnet;
					newMagnet = Magnet(rdListMagnets.size());
					newMagnet.setType(5);
					newMagnet.setPposition(sf::Vector2f(x, y));
					newMagnet.setDirection(direction);
					rdListMagnets.push_back(newMagnet);
				}
				else if(lineRead.find("platform") < lineRead.size())
				{
					float x = 0, y = 0;
					if(lineRead.find("x=") < lineRead.size())
					{
						x += atof(&lineRead[lineRead.find("x=") + 3]);
					}

					if(lineRead.find("y=") < lineRead.size())
					{
						y += atof(&lineRead[lineRead.find("y=") + 3]);
					}
					Platform newPlatform;
					newPlatform = Platform();
					newPlatform.setType(6);
					newPlatform.setPposition(sf::Vector2f(x, y));
					rdListPlatforms.push_back(newPlatform);
				}
				else if(lineRead.find("box") < lineRead.size())
				{
					float width = 0, height = 0;
					int type = 0;
					if(lineRead.find("width=") < lineRead.size())
					{
						width += atof(&lineRead[lineRead.find("width=") + 7]);
						if(lineRead.find("height=") < lineRead.size())
						{
							height += atof(&lineRead[lineRead.find("height=") + 8]);
						}

						if(lineRead.find("type=") < lineRead.size())
						{
							type += atof(&lineRead[lineRead.find("type=") + 6]);
						}
						Box newBox;
						newBox = Box(rdListMagnets);
						newBox.setType(type);
						newBox.setScale(sf::Vector2f(width, height));
						newBox.setPposition(sf::Vector2f(100, 200));
						rdListBoxes.push_back(newBox);
					}
				}
			}
			myFileRead.close();
		}

	}

	void write(std::string fileName)
	{
		int minScore = 0;
		//std::cout << "Minimalni skore pro zlatou medaili (predvypocitane skore: " << (wrListBoxes.size() * 3000 - 1000) << "). Pokud s tim souhlasite, tak zadejte 0 :";
		//std::cin >> minScore;
		fileName = "levels/" + fileName;
		if(minScore == 0)
		{
			myFileWrite.open(fileName.c_str());
			myFileWrite << "<?xml version='1.0' encoding='utf-8'?>" << std::endl << "<settings boxes='" << wrListBoxes.size() << "' min='" << (wrListBoxes.size() * 3000 - 1000) << "'>" << std::endl;
		}
		else
		{
			myFileWrite.open(fileName.c_str());
			myFileWrite << "<?xml version='1.0' encoding='utf-8'?>" << std::endl << "<settings boxes='" << wrListBoxes.size() << "' min='" << minScore << "'>" << std::endl;
		}
		for (std::vector<Physical>::iterator it = wrListPlatforms.begin(); it != wrListPlatforms.end(); ++it)
		{
			Physical &dPlatform = *it;
			myFileWrite << "<platform x='" << (dPlatform.getPosition().x - dPlatform.getTextureRect().width / 2) << "' y='" << (dPlatform.getPosition().y - 35 - dPlatform.getTextureRect().height / 2) << "'/>" << std::endl;
		}
		for (std::vector<Magnet>::iterator it = wrListMagnets.begin(); it != wrListMagnets.end(); ++it)
		{
			Magnet &dMagnet = *it;
			myFileWrite << "<magnet x='" << (dMagnet.getPosition().x - dMagnet.getTextureRect().width / 2) << "' y='" << (dMagnet.getPosition().y - 35 - dMagnet.getTextureRect().height / 2) << "' direction='" << dMagnet.getDirection() << "'/>" << std::endl;
		}
		for (std::vector<Moveable>::iterator it = wrListCheckers.begin(); it != wrListCheckers.end(); ++it)
		{
			Moveable &dChecker = *it;
			myFileWrite << "<checker x='" << (dChecker.getPosition().x - dChecker.getTextureRect().width / 2) << "' y='" << (dChecker.getPosition().y - 35 - dChecker.getTextureRect().height / 2) << "'/>" << std::endl;
		}
		for (std::vector<Box>::iterator it = wrListBoxes.begin(); it != wrListBoxes.end(); ++it)
		{
			Box &dBox = *it;
			myFileWrite << "<box width='" << abs(dBox.getScale().x) << "' height='" << abs(dBox.getScale().y) << "' type='" << dBox.getType() <<  "'/>" << std::endl;
		}
		myFileWrite << "</settings>";
		myFileWrite.close();
		wrListPlatforms.clear();
		wrListCheckers.clear();
		wrListBoxes.clear();
		wrListMagnets.clear();
	}
};
class Eventable
{
protected:
	static sf::Event* event;
	static sf::RenderWindow* window;
public:
	static void setWindow(sf::RenderWindow &windo)
	{
		window = &windo;
	}

	static void setEvent(sf::Event &even)
	{
		event = &even;
	}
};
class Item : public Eventable
{
private:
	sf::RectangleShape border;
	Moveable mItem;
	bool selected;
public:
	Item()
	{
		border.setFillColor(sf::Color::White);
		border.setOutlineThickness(2.0f);
		border.setOutlineColor(sf::Color::Black);
		selected = false;
		mItem = Moveable();
	}

	Item(sf::Vector2f pPosition, Moveable pItem)
	{
		border.setPosition(pPosition);
		border.setFillColor(sf::Color::White);
		border.setOutlineThickness(2.0f);
		border.setOutlineColor(sf::Color::White);
		std::cout << "Px: " << pPosition.x << " Py: " << pPosition.y << std::endl;
		selected = false;
		mItem = Moveable(pItem.getType());
		mItem.setTexture(*pItem.getTexture());
		//mBox.setPposition(pPosition, true);
	}

	void setPosition(sf::Vector2f pPosition)
	{
		border.setPosition(pPosition);
		mItem.setPposition(sf::Vector2f(border.getPosition().x + (border.getSize().x * border.getScale().x) / 2, border.getPosition().y + (border.getSize().y * border.getScale().y) / 2));
	}

	void setSize(sf::Vector2f pSize)
	{
		border.setSize(sf::Vector2f(pSize.x - 2, pSize.y - 4));
		mItem.setPposition(sf::Vector2f(border.getPosition().x + (border.getSize().x * border.getScale().x) / 2, border.getPosition().y + (border.getSize().y * border.getScale().y) / 2));
	}

	sf::Vector2f getSize()
	{
		return border.getSize();
	}

	void setItemScale(sf::Vector2f pScale)
	{
		mItem.setScale(pScale);
	}

	void select()
	{
		if(selected == false)
		{
			border.setOutlineThickness(2.0f);
			border.setOutlineColor(sf::Color::Green);
			selected = true;
			//break;
		}
		else
		{
			border.setOutlineThickness(2.0f);
			border.setOutlineColor(border.getFillColor());
			selected = false;
			//break;
		}
	}

	bool clickDown()
	{
		if ((event->type == sf::Event::MouseButtonPressed) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > border.getPosition().x && MousePos.y > border.getPosition().y &&
			MousePos.x < (border.getPosition().x + border.getSize().x) &&
			MousePos.y < (border.getPosition().y + border.getSize().y))
			{
				
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	Moveable getItem()
	{
		return mItem;
	}

	bool getSelect()
	{
		return selected;
	}

	void draw()
	{
		window->draw(border);
		window->draw(mItem);
		//std::cout << "Proc?" << std::endl;
	}
};
class Line : public Eventable
{
private:
	sf::VertexArray line;
	int width;
public:
	Line()
	{
		line.setPrimitiveType(sf::Lines);
		width = 1;
	}
	 
	void setWidth(int pWidth)
	{
		width = pWidth;
	}

	Line(sf::Vector2f firstPosition, sf::Vector2f secondPosition)
	{
		line.setPrimitiveType(sf::Lines);
		float x1, x2, y1, y2;
		x1 = firstPosition.x;
		x2 = secondPosition.x;
		y1 = firstPosition.y;
		y2 = secondPosition.y;
		for(int i = 0; i < width; i++)
		{
			if(firstPosition.x == secondPosition.x)
			{
				firstPosition.x = x1 + i;
				secondPosition.x = x2 + i;
			}
			else if(firstPosition.y == secondPosition.y)
			{
				firstPosition.y = y1 + i;
				secondPosition.y = y2 + i;
			}
			line.append(sf::Vertex(firstPosition));
			line.append(sf::Vertex(secondPosition));
		}
	}

	void setPosition(sf::Vector2f firstPosition, sf::Vector2f secondPosition)
	{
		line.clear();
		line.setPrimitiveType(sf::Lines);
		float x1, x2, y1, y2;
		x1 = firstPosition.x;
		x2 = secondPosition.x;
		y1 = firstPosition.y;
		y2 = secondPosition.y;
		for(int i = 0; i < width; i++)
		{
			if(firstPosition.x == secondPosition.x)
			{
				firstPosition.x = x1 + i;
				secondPosition.x = x2 + i;
			}
			else if(firstPosition.y == secondPosition.y)
			{
				firstPosition.y = y1 + i;
				secondPosition.y = y2 + i;
			}
			line.append(sf::Vertex(firstPosition));
			line.append(sf::Vertex(secondPosition));
		}
	}

	void setWidth(float widt)
	{

	}

	void draw()
	{
		window->draw(line);
	}
};
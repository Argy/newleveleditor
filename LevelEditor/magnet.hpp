class Magnet : public Physical
{
protected:
	short direction;
	bool active;
	int id;
public:
	Magnet()
	{
		id = 0;
		active = false;
		direction = 1;
	}

	Magnet(int pId)
	{
		id = pId;
		active = false;
		direction = 1;
	}

	Magnet(sf::Vector2f mouse, int pId)
	{
		id = pId;
		direction = 1;
		active = false;
		this->setPposition(mouse);
	}

	void setId(int pId)
	{
		id = pId;
	}

	int getId()
	{
		return id;
	}

	void setActive(bool state)
	{
		active = state;
	}

	bool isActive()
	{
		return active;
	}

	short getDirection()
	{
		return direction;
	}

	void setDirection(short dir)
	{
		direction = dir;
	}
};
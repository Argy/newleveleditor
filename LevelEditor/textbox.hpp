class TextBox : public Eventable
{
private:
	sf::Text text;
	sf::String buffer, mezibuffer;
	sf::RectangleShape borderOutline;
	sf::RectangleShape borderInside;
	int maxChars;
	bool active;
public:
	TextBox()
	{
		buffer = "";
		mezibuffer = "";
		maxChars = 20;
		active = false;
		text.setCharacterSize(20);
		text.setFont(arial);
		text.setColor(sf::Color::Black);
		borderOutline.setSize(sf::Vector2f(250, 35));
		borderOutline.setOutlineColor(sf::Color::Black);
		borderOutline.setFillColor(sf::Color(50, 50, 50));
		borderOutline.setOutlineThickness(1);
		borderInside.setSize(sf::Vector2f(borderOutline.getSize().x - 4, borderOutline.getSize().y - 4));
		borderInside.setOutlineColor(sf::Color::Black);
		borderInside.setFillColor(sf::Color(200, 200, 200));
		borderInside.setOutlineThickness(1);

	}

	sf::Vector2f getPosition()
	{
		return borderOutline.getPosition();
	}

	sf::Vector2f getSize()
	{
		return borderOutline.getSize();
	}

	void setPosition(sf::Vector2f pos)
	{
		borderOutline.setPosition(pos);
		borderInside.setPosition(pos.x + 2, pos.y + 2);
		text.setPosition(pos.x + 6, pos.y + 6);
	}

	void writing()
	{
		if(active)
		{
			if ((event->type == sf::Event::KeyPressed) && (event->key.code == sf::Keyboard::BackSpace))
			{
				if(buffer.getSize() > 0)
				{
					if(buffer.getSize() > maxChars)
						mezibuffer.insert(0, buffer[buffer.getSize() - (maxChars + 1)]);
					buffer.erase(buffer.getSize() - 1);
					mezibuffer.erase(mezibuffer.getSize() - 1);
					text.setString(mezibuffer);
				}
			}

			if ((event->type == sf::Event::TextEntered))
			{
				if (event->text.unicode != 8 && event->text.unicode != 9 && event->text.unicode != 13)
				{
					buffer += static_cast<char>(event->text.unicode);
					mezibuffer += static_cast<char>(event->text.unicode);
				}
				if(mezibuffer.getSize() > maxChars)
				{
					mezibuffer.erase(0);
				}
				text.setString(mezibuffer);
			}
		}
	}

	bool onClick()
	{
		if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > borderOutline.getPosition().x &&
				MousePos.y > borderOutline.getPosition().y &&
				MousePos.x < (borderOutline.getPosition().x + borderOutline.getSize().x) &&
				MousePos.y < (borderOutline.getPosition().y + borderOutline.getSize().y))
			{
				if(!this->isActive())
					this->setActive(true);
				return true;
			}
			else
			{
				if(this->isActive())
					this->setActive(false);
				return false;
			}
		}
	}

	bool mouseOnObject()
	{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseMove.x, event->mouseMove.y);
			if((MousePos.x > borderOutline.getPosition().x &&
				MousePos.y > borderOutline.getPosition().y &&
			MousePos.x < (borderOutline.getPosition().x + borderOutline.getSize().x) &&
			MousePos.y < (borderOutline.getPosition().y + borderOutline.getSize().y)))
			{
				return true;
			}
			else
			{
				return false;
			}
	}

	void setActive(bool activ)
	{
		active = activ;
		if(active)
			borderInside.setFillColor(sf::Color::White);
		else
			borderInside.setFillColor(sf::Color(200, 200, 200));
	}

	bool isActive()
	{
		return active;
	}

	sf::String getString()
	{
		return buffer;
	}

	void setText(sf::String texticek)
	{
		buffer = texticek;
		mezibuffer = buffer;
		text.setString(mezibuffer);
	}

	void draw()
	{
		window->draw(borderOutline);
		window->draw(borderInside);
		window->draw(text);
	}
};
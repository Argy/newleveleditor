class Grid : public Eventable
{
private:
	sf::Vector2f space;
	std::vector<Line> gridX, gridY;
	bool status;
public:
	Grid()
	{
		space.x = 32;
		space.y = 32;
		status = false;
	}

	void setStatus(bool pStatus)
	{
		status = pStatus;
	}

	bool getStatus()
	{
		return status;
	}

	void setSpace(sf::Vector2f pSpace)
	{
		space = pSpace;
		int maxI = window->getSize().y / space.y;
		int maxJ = (window->getSize().x - 100) / space.x;
		
		for(int i = 1; i < maxI + 1; i++)
		{
			Line lineX;
			lineX.setPosition(sf::Vector2f(0, 35 + i * space.y), sf::Vector2f(800, 35 + i * space.y));
			gridX.push_back(lineX);
		}
		for(int j = 1; j < maxJ + 1; j++)
		{
			Line lineY;
			lineY.setPosition(sf::Vector2f(j * space.x, 35), sf::Vector2f(j * space.x, 515));
			gridY.push_back(lineY);
		}
	}

	void drawLines()
	{
		if(status == true)
		{
			for (std::vector<Line>::iterator it = gridX.begin(); it != gridX.end(); ++it)
			{
				Line &dLine = *it;
				dLine.draw();
			}

			for (std::vector<Line>::iterator it = gridY.begin(); it != gridY.end(); ++it)
			{
				Line &dLine = *it;
				dLine.draw();
			}
		}
	}
};
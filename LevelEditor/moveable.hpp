class Moveable : public Eventable, public sf::Sprite
{
protected:
	bool drag, mVisibleObjects;
	int type;
	sf::Vector2f dragPos;
	static sf::Texture textureDelete, textureFlip;
	sf::Sprite spriteDelete, spriteFlip;
public:
	Moveable()
	{
		spriteDelete.setTexture(textureDelete);
		spriteFlip.setTexture(textureFlip);
		drag = false;
		mVisibleObjects = false;
		type = 0;
		spriteDelete.setPosition(this->getPosition().x + this->getTextureRect().width * this->getScale().x / 2, this->getPosition().y + this->getTextureRect().height * this->getScale().y / 2);
		spriteFlip.setPosition(this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2 - spriteFlip.getTextureRect().width / 2, this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 - spriteFlip.getTextureRect().height / 2);
		this->setOrigin(this->getTextureRect().width / 2, this->getTextureRect().height / 2);
	}

	Moveable(int typ)
	{
		spriteDelete.setTexture(textureDelete);
		spriteFlip.setTexture(textureFlip);
		drag = false;
		mVisibleObjects = false;
		type = typ;
		spriteDelete.setPosition(this->getPosition().x + this->getTextureRect().width * this->getScale().x / 2, this->getPosition().y + this->getTextureRect().height * this->getScale().y / 2);
		spriteFlip.setPosition(this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2 - spriteFlip.getTextureRect().width / 2, this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 - spriteFlip.getTextureRect().height / 2);
		this->setOrigin(this->getTextureRect().width / 2, this->getTextureRect().height / 2);
	}

	int getType()
	{
		return type;
	}

	void setType(int typ)
	{
		type = typ;
	}

	void setPposition(sf::Vector2f pPosition)
	{
		this->setOrigin(this->getTextureRect().width / 2, this->getTextureRect().height / 2);
		/*if(center)
		{
			this->setPosition(pPosition.x - this->getTextureRect().width / 2, pPosition.y - this->getTextureRect().height / 2);
			spriteDelete.setPosition(this->getPosition().x - spriteDelete.getTextureRect().width / 2, this->getPosition().y - spriteDelete.getTextureRect().height / 2);
		}
		else
		{*/
			this->setPosition(pPosition.x, pPosition.y);
			spriteDelete.setPosition(this->getPosition().x - this->getTextureRect().width * this->getScale().x / 2 - spriteDelete.getTextureRect().width / 2, this->getPosition().y - this->getTextureRect().height * this->getScale().y / 2 - spriteDelete.getTextureRect().height / 2);
			spriteFlip.setPosition(this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2 - spriteFlip.getTextureRect().width / 2, this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 - spriteFlip.getTextureRect().height / 2);
		//}
	}

	bool mouseOnObject()
	{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseMove.x, event->mouseMove.y);
			if((MousePos.x > this->getPosition().x -  this->getTextureRect().width * abs(this->getScale().x) / 2 &&
				MousePos.y > this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 &&
			MousePos.x < (this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2) &&
			MousePos.y < (this->getPosition().y + this->getTextureRect().height * abs(this->getScale().y) / 2)) ||
			(MousePos.x > spriteDelete.getPosition().x && MousePos.y > spriteDelete.getPosition().y &&
			MousePos.x < (spriteDelete.getPosition().x + spriteDelete.getTextureRect().width) &&
			MousePos.y < (spriteDelete.getPosition().y + spriteDelete.getTextureRect().height)) ||
			(MousePos.x > spriteFlip.getPosition().x && MousePos.y > spriteFlip.getPosition().y &&
			MousePos.x < (spriteFlip.getPosition().x + spriteFlip.getTextureRect().width) &&
			MousePos.y < (spriteFlip.getPosition().y + spriteFlip.getTextureRect().height)))
			{
				this->setColor(sf::Color::Color(this->getColor().r, this->getColor().g, this->getColor().b, 125));
				mVisibleObjects = true;
				return true;
			}
			else
			{
				if(this->getColor().a == 125)
				{
					this->setColor(sf::Color::Color(this->getColor().r, this->getColor().g, this->getColor().b, 255));
					mVisibleObjects = false;
				}
				return false;
			}
	}

	bool clickUp()
	{
		if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > this->getPosition().x -  this->getTextureRect().width * abs(this->getScale().x) / 2 &&
				MousePos.y > this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 &&
			MousePos.x < (this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2) &&
			MousePos.y < (this->getPosition().y + this->getTextureRect().height * abs(this->getScale().y) / 2))
			{
				drag = false;
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	bool clickDown()
	{
		if ((event->type == sf::Event::MouseButtonPressed) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > this->getPosition().x -  this->getTextureRect().width * abs(this->getScale().x) / 2 &&
				MousePos.y > this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 &&
			MousePos.x < (this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2) &&
			MousePos.y < (this->getPosition().y + this->getTextureRect().height * abs(this->getScale().y) / 2))
			{
				drag = true;
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	bool clickDown2()
	{
		if ((event->type == sf::Event::MouseButtonPressed) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > this->getPosition().x -  this->getTextureRect().width * abs(this->getScale().x) / 2 &&
				MousePos.y > this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 &&
				MousePos.x < (this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2) &&
				MousePos.y < (this->getPosition().y + this->getTextureRect().height * abs(this->getScale().y) / 2))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	bool mouseMove()
	{
		if ((event->type == sf::Event::MouseMoved) && (drag == true))
		{
			sf::Vector2f mousePos = sf::Vector2f(event->mouseMove.x, event->mouseMove.y);
			this->setPosition(mousePos.x, mousePos.y);
			spriteDelete.setPosition(this->getPosition().x - this->getTextureRect().width * abs(this->getScale().x) / 2 - spriteDelete.getTextureRect().width / 2, this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 - spriteDelete.getTextureRect().height / 2);
			spriteFlip.setPosition(this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2 - spriteFlip.getTextureRect().width / 2, this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 - spriteFlip.getTextureRect().height / 2);
			return true;
		}
		else
		{
			return false;
		}
	}

	const static bool staticInitDelete(std::string imageDelete) /////////////////////////////////nacteme pro vsechny objekty stejny obrazek -- strelu
    {
		return textureDelete.loadFromFile(imageDelete);
    }

	const static bool staticInitFlip(std::string imageFlip) /////////////////////////////////nacteme pro vsechny objekty stejny obrazek -- strelu
    {
		return textureFlip.loadFromFile(imageFlip);
    }

	bool clickDeleteUp()
	{
		if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > spriteDelete.getPosition().x && MousePos.y > spriteDelete.getPosition().y &&
			MousePos.x < (spriteDelete.getPosition().x + spriteDelete.getTextureRect().width) &&
			MousePos.y < (spriteDelete.getPosition().y + spriteDelete.getTextureRect().height))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	bool clickFlipUp()
	{
		if ((event->type == sf::Event::MouseButtonReleased) && (event->mouseButton.button == sf::Mouse::Left))
		{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseButton.x, event->mouseButton.y);
			if(MousePos.x > spriteFlip.getPosition().x && MousePos.y > spriteFlip.getPosition().y &&
			MousePos.x < (spriteFlip.getPosition().x + spriteFlip.getTextureRect().width) &&
			MousePos.y < (spriteFlip.getPosition().y + spriteFlip.getTextureRect().height))
			{
				this->setScale(-this->getScale().x, this->getScale().y);
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	void draw()
	{
		window->draw(*this);
		if(mVisibleObjects == true)
		{
			window->draw(spriteDelete);
			window->draw(spriteFlip);
		}
	}
};
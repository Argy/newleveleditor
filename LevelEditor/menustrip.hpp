class MenuStrip : public Eventable
{
private:
	Button mainButton;
	Button buttons[6];
	bool show;
public:
	MenuStrip()
	{
		show = false;
		mainButton = Button(1);
		mainButton.setPosition(sf::Vector2f(0, 0));
		mainButton.setSize(sf::Vector2f(100, 35));
		mainButton.setTitle("Program");
		buttons[0] = Button(1);
		buttons[0].setPosition(sf::Vector2f(0, 35));
		buttons[0].setSize(sf::Vector2f(200, 35));
		buttons[0].setTitle("Zmena pozadi      F2");
		buttons[1] = Button(1);
		buttons[1].setPosition(sf::Vector2f(0, 70));
		buttons[1].setSize(sf::Vector2f(200, 35));
		buttons[1].setTitle("Fyzika            F3");
		buttons[2] = Button(1);
		buttons[2].setPosition(sf::Vector2f(0, 105));
		buttons[2].setSize(sf::Vector2f(200, 35));
		buttons[2].setTitle("Vymazat scenu     F4");
		buttons[3] = Button(1);
		buttons[3].setPosition(sf::Vector2f(0, 140));
		buttons[3].setSize(sf::Vector2f(200, 35));
		buttons[3].setTitle("Nacist scenu      F5");
		buttons[4] = Button(1);
		buttons[4].setPosition(sf::Vector2f(0, 175));
		buttons[4].setSize(sf::Vector2f(200, 35));
		buttons[4].setTitle("Vybrat bedny      F7");
		buttons[5] = Button(1);
		buttons[5].setPosition(sf::Vector2f(0, 210));
		buttons[5].setSize(sf::Vector2f(200, 35));
		buttons[5].setTitle("Ulozit scenu      F8");
	}

	void control()
	{
		mainButton.mouseOnObject();
		if(mainButton.clickDown())
		{
			if(show)
			{
				show = false;
			}
			else
			{
				show = true;
			}
		}
		for(int i = 0; i < 6; i++)
		{
			buttons[i].mouseOnObject();
			if(buttons[i].clickDown())
			{
				show = false;
			}
		}
	}

	bool buttonClickDown(int number)
	{
		return buttons[number].clickDown();
	}

	bool isShow()
	{
		return show;
	}

	void drawAll()
	{
		mainButton.draw();
		if(show)
		{
			for(int i = 0; i < 6; i++)
			{
				buttons[i].draw();
			}
		}
	}
};
class Dialog : public Eventable
{
private:
	sf::Text label;
	sf::RectangleShape border;
	TextBox textBox;
	bool active;
	Hint hint;
	Button button;
public:
	Dialog()
	{
		hint = Hint();
		active = false;
		label.setCharacterSize(25);
		border.setSize(sf::Vector2f(450, 100));
		border.setOutlineColor(sf::Color::Black);
		border.setFillColor(sf::Color(100, 100, 100));
		border.setOutlineThickness(1);
		label.setFont(arial);
		label.setColor(sf::Color::White);
		label.setString("Trololo");
		button = Button();
		button.setSize(sf::Vector2f(100, 35));
		button.setTitle("Potvrdit");
	}

	bool mouseOnObject()
	{
			sf::Vector2f MousePos = sf::Vector2f(event->mouseMove.x, event->mouseMove.y);
			if((MousePos.x > border.getPosition().x -  border.getSize().x &&
				MousePos.y > border.getPosition().y - border.getSize().y &&
			MousePos.x < (border.getPosition().x + border.getSize().x) &&
			MousePos.y < (border.getPosition().y + border.getSize().y)))
			{
				hint.show(true);
				return true;
			}
			else
			{
				hint.show(false);
				return false;
			}
	}

	void setPosition(sf::Vector2f pos)
	{
		border.setPosition(pos);
		label.setPosition(pos.x + 20, pos.y + 10);
		textBox.setPosition(sf::Vector2f(pos.x + 50, pos.y + 60));
		hint.setString("Zadejte nazev levelu. Napr. level-001-015.lvl");
		hint.setPosition(sf::Vector2f(border.getPosition().x + 20, border.getPosition().y - 40));
		button.setPosition(sf::Vector2f(border.getPosition().x + border.getSize().x - button.getSize().x - 5, 
		border.getPosition().y + border.getSize().y - button.getSize().y - 5));
	}

	void setLabel(sf::String string)
	{
		label.setString(string);
	}

	void setButtonText(sf::String string)
	{
		button.setTitle(string);
	}

	bool onClick()
	{
		return textBox.onClick();
	}

	bool onButtonClick()
	{
		return button.clickDown();
	}

	void draw()
	{
		window->draw(border);
		window->draw(label);
		textBox.draw();
		button.draw();
		hint.draw();
	}

	void writing()
	{
		textBox.writing();
	}

	bool isTextBoxActive()
	{
		return textBox.isActive();
	}

	bool isActive()
	{
		return active;
	}

	void setActive(bool activ)
	{
		active = activ;
		if(!active)
			textBox.setActive(false);
	}

	sf::String getString()
	{
		return textBox.getString();
	}
};
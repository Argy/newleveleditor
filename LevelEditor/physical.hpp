class Physical : public Moveable
{
protected:
	b2BodyDef bodyDef;
	b2Body* body;
	b2PolygonShape shape;
	b2FixtureDef fixtureDef;
	float SCALE;
	bool mBody;
public:
	Physical()
	{
		SCALE = 30.f;
		mBody = false;
	}

	void startPhysics(b2World& world, b2BodyType bodyTyp)
	{
		mBody = true;
		bodyDef.position = b2Vec2(this->getPosition().x/SCALE, this->getPosition().y/SCALE);
		bodyDef.type = bodyTyp;
		body = world.CreateBody(&bodyDef);
		if(this->getType() == 6)
			shape.SetAsBox((this->getTextureRect().width * abs(this->getScale().x)/2)/SCALE, (this->getTextureRect().height * abs(this->getScale().y)/2 - 10)/SCALE);
		else
			shape.SetAsBox((this->getTextureRect().width * abs(this->getScale().x)/2)/SCALE, (this->getTextureRect().height * abs(this->getScale().y)/2)/SCALE);
		switch(this->getType())
		{
		case 0:
			fixtureDef.density = 2.f;
			fixtureDef.friction = 0.5f;
			break;
		case 1:
			fixtureDef.density = 3.f;
			fixtureDef.friction = 0.4f;
			break;
		case 3:
			fixtureDef.density = 8.f;
			fixtureDef.friction = 0.5f;
			break;
		case 5:
			fixtureDef.density = 10.f;
			fixtureDef.friction = 0.8f;
			break;
		case 6:
			fixtureDef.density = 20.f;
			fixtureDef.friction = 0.8f;
			break;
		default:
			fixtureDef.density = 2.f;
			fixtureDef.friction = 0.5f;
			break;
		}
		fixtureDef.shape = &shape;
		body->CreateFixture(&fixtureDef);
	}

	void destroyBody(b2World& world)
	{
		mBody = false;
		world.DestroyBody(body);
	}

	bool isBody()
	{
		return mBody;
	}

	void update()
	{
        this->setOrigin(this->getTextureRect().width / 2, this->getTextureRect().height / 2);
        this->setPosition(SCALE * body->GetPosition().x, SCALE * body->GetPosition().y);
        this->setRotation(body->GetAngle() * 180/b2_pi);
		spriteDelete.setPosition(this->getPosition().x - this->getTextureRect().width / 2 - spriteDelete.getTextureRect().width / 2, this->getPosition().y - this->getTextureRect().height / 2 - spriteDelete.getTextureRect().height / 2);
		spriteFlip.setPosition(this->getPosition().x + this->getTextureRect().width * abs(this->getScale().x) / 2 - spriteFlip.getTextureRect().width / 2, this->getPosition().y - this->getTextureRect().height * abs(this->getScale().y) / 2 - spriteFlip.getTextureRect().height / 2);
	}
};
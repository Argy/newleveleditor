class Box : public Physical
{
protected:
	bool metal, magnet;
	int magnetDirection;
	std::vector<Magnet> magnets;
	float distance;
public:
	Box()
	{
		magnetDirection = 0;
		metal = false;
		magnet = false;
		distance = 0;
	}

	Box(std::vector<Magnet> pMagnets)
	{
		magnets = pMagnets;
		magnetDirection = 0;
		metal = false;
		magnet = false;
		distance = 0;
	}

	Box(sf::Vector2f mouse)
	{
		this->setPposition(mouse);
		metal = false;
		magnet = false;
		distance = 0;
	}

	Box(sf::Vector2f mouse, std::vector<Magnet> pMagnets)
	{
		magnets = pMagnets;
		this->setPposition(mouse);
		metal = false;
		magnet = false;
		distance = 0;
	}

	void addMagnet(Magnet pMagnet)
	{
		magnets.push_back(pMagnet);
	}

	void flipMagnet(int pId)
	{
		for (std::vector<Magnet>::iterator it = magnets.begin(); it != magnets.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(dMagnet.getId() == pId)
			{
				dMagnet.setDirection(dMagnet.getDirection() * (-1));
			}
		}
	}

	void setPosMagnet(int pId, sf::Vector2f newPos)
	{
		for (std::vector<Magnet>::iterator it = magnets.begin(); it != magnets.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(dMagnet.getId() == pId)
			{
				dMagnet.setPosition(newPos);
			}
		}
	}

	void deleteMagnet(int pId)
	{
		for (std::vector<Magnet>::iterator it = magnets.begin(); it != magnets.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(dMagnet.getId() == pId)
			{
				magnets.erase(it);
				break;
			}
		}
	}

	void magnetism(bool stage)
	{
		if(magnet)
		{
			magnetDirection = 0;
			for (std::vector<Magnet>::iterator it = magnets.begin(); it != magnets.end(); ++it)
			{
				Magnet &dMagnet = *it;
				if(dMagnet.isActive())
				{
					magnetDirection = dMagnet.getDirection();
					if(abs(dMagnet.getPosition().x - this->getPosition().x) / 100 != 0)
					{
						distance = abs(dMagnet.getPosition().x - this->getPosition().x) / 100;
					}
					else {
						distance = 4.0f;
					}
				
					if(distance < 2.0f)
					{
						distance = 2.0f;
					}
					if(!stage)
						body->ApplyForce(b2Vec2(650 * magnetDirection / distance, 0), body->GetPosition());
					else
						body->ApplyForce(b2Vec2(250 * magnetDirection / distance, 0), body->GetPosition());
				}
			}
		}
	}

	void underWaterEffect()
	{
		//body->SetLinearDamping(0.1f);
		//body->ApplyForce(b2Vec2(body->GetLinearVelocity().x * (-100), body->GetLinearVelocity().y * (-100)), body->GetPosition());
	}

	void magnetOn(int pId, int magnDirection, float dist)
	{
		for (std::vector<Magnet>::iterator it = magnets.begin(); it != magnets.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(dMagnet.getId() == pId)
			{
				if(mBody && !dMagnet.isActive())
				{
					dMagnet.setActive(true);
					if(!magnet)
					{
						magnet = true;
					}
				}
			}
		}
	}
	
	void magnetOff(int pId, int magnDirection)
	{
		int hack = 0;
		for (std::vector<Magnet>::iterator it = magnets.begin(); it != magnets.end(); ++it)
		{
			Magnet &dMagnet = *it;
			if(dMagnet.isActive())
				hack += dMagnet.getDirection();
			if(dMagnet.getId() == pId)
			{
				if(mBody && dMagnet.isActive())
				{
					dMagnet.setActive(false);
					hack -= dMagnet.getDirection();
				}
			}
		}
		if(hack == 0 && magnet)
		{
			magnet = false;
		}
	}

	bool getMagnetism()
	{
		return magnet;
	}
};